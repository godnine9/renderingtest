// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/QBHair_v2"
{
	Properties
	{
		[NoScaleOffset]_StarTexture("StarTexture", 2D) = "gray" {}
		_DepthScale("Depth Scale", Range( 0 , 1)) = 0.3
		_LightColor("LightColor", Color) = (1,1,1,0)
		_LightPower("LightPower", Range( 0 , 3)) = 0.27
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_StarTintColor("StarTintColor", Color) = (0,0,0,0)
		_BaseColor("BaseColor", Color) = (0,0,0,0)
		_AddIntensity("AddIntensity", Range( 1 , 3)) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "Lighting.cginc"
		#pragma target 4.6
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 viewDir;
			INTERNAL_DATA
			float3 worldPos;
			float3 worldNormal;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform float4 _BaseColor;
		uniform sampler2D _StarTexture;
		uniform float _DepthScale;
		uniform float4 _StarTintColor;
		uniform float4 _LightColor;
		uniform float _LightPower;
		uniform sampler2D _TextureSample0;
		uniform float4 _TextureSample0_ST;
		uniform float _AddIntensity;

		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			c.rgb = 0;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
			float2 Offset272 = ( ( 0.0 - 1 ) * i.viewDir.xy * _DepthScale ) + i.uv_texcoord;
			float2 ParalaxUV310 = Offset272;
			float2 panner268 = ( ( _SinTime.x * 0.5 ) * float2( 0.2,0 ) + ParalaxUV310);
			float4 tex2DNode76 = tex2D( _StarTexture, panner268 );
			float lerpResult345 = lerp( 0.0 , ( ( ( sin( _Time.w ) + 1.0 ) * 0.3 ) + 0.3 ) , tex2DNode76.r);
			float4 lerpResult320 = lerp( _BaseColor , ( lerpResult345 + ( tex2DNode76.r * _StarTintColor ) ) , tex2DNode76.r);
			float2 panner306 = ( _SinTime.x * float2( 0,0.15 ) + ParalaxUV310);
			float4 tex2DNode305 = tex2D( _StarTexture, panner306 );
			float lerpResult362 = lerp( 0.0 , ( ( ( sin( _Time.w ) + 1.0 ) * 0.12 ) + 0.12 ) , tex2DNode305.g);
			float4 lerpResult321 = lerp( lerpResult320 , ( ( tex2DNode305.g * _StarTintColor ) + lerpResult362 ) , ( tex2DNode305.g * 0.5 ));
			float2 panner313 = ( _SinTime.x * float2( 0.1,0.2 ) + ParalaxUV310);
			float4 tex2DNode314 = tex2D( _StarTexture, panner313 );
			float lerpResult365 = lerp( 0.0 , ( ( ( sin( _Time.w ) + 1.0 ) * 0.05 ) + 0.3 ) , tex2DNode314.b);
			float4 lerpResult322 = lerp( lerpResult321 , ( lerpResult365 + ( tex2DNode314.b * _StarTintColor ) ) , ( tex2DNode314.b * 0.1 ));
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float fresnelNdotV298 = dot( ase_worldNormal, ase_worldViewDir );
			float fresnelNode298 = ( 0.0 + 1.0 * pow( 1.0 - fresnelNdotV298, 5.0 ) );
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			float lerpResult300 = lerp( fresnelNode298 , 1.0 , tex2D( _TextureSample0, uv_TextureSample0 ).r);
			float4 lerpResult297 = lerp( lerpResult322 , ( _LightColor * _LightPower ) , lerpResult300);
			float4 BaseColor253 = lerpResult297;
			o.Emission = ( BaseColor253 * _AddIntensity ).rgb;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows exclude_path:deferred 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 4.6
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.viewDir = IN.tSpace0.xyz * worldViewDir.x + IN.tSpace1.xyz * worldViewDir.y + IN.tSpace2.xyz * worldViewDir.z;
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17800
0;0;1920;1019;-4029.432;-458.0715;1;True;True
Node;AmplifyShaderEditor.CommentaryNode;312;481.4344,241.105;Inherit;False;893.0659;572.4729;Comment;5;273;267;274;272;310;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;390;1411.831,-48.78596;Inherit;False;3487.458;1660.567;Comment;44;375;380;376;308;381;377;311;340;379;378;268;316;306;382;76;358;345;317;313;383;305;314;325;362;315;344;318;329;320;319;326;365;356;321;352;330;322;297;253;384;385;386;387;388;;1,1,1,1;0;0
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;273;559.7184,625.5781;Float;False;Tangent;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;274;531.4343,515.6041;Float;False;Property;_DepthScale;Depth Scale;1;0;Create;True;0;0;False;0;0.3;0.065;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;267;548.6093,291.1051;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ParallaxMappingNode;272;915.5776,507.0912;Inherit;False;Normal;4;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT3;0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TimeNode;375;2107.351,1.214034;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;310;1150.501,515.346;Inherit;False;ParalaxUV;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SinTimeNode;308;1461.831,675.0608;Inherit;True;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SinOpNode;376;2288.506,35.0739;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TimeNode;380;2084.354,828.6777;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;311;1656.678,512.9827;Inherit;False;310;ParalaxUV;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;377;2406.435,28.57135;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TimeNode;384;1887.589,1428.781;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SinOpNode;381;2265.509,862.5377;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;340;1685.195,285.395;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;379;2383.438,856.035;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;268;1924.476,164.015;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0.2,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SinOpNode;385;2068.744,1462.641;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;378;2599.435,47.57135;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.3;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;316;2712.428,537.3116;Inherit;False;Property;_StarTintColor;StarTintColor;5;0;Create;True;0;0;False;0;0,0,0,0;0.809287,0.508366,0.9056604,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;382;2517.438,853.035;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.12;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;358;2795.756,43.86133;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.3;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;386;2186.672,1456.138;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;306;1930.282,638.1542;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0.15;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;76;2182.66,276.6936;Inherit;True;Property;_StarTexture;StarTexture;0;1;[NoScaleOffset];Create;True;0;0;False;0;-1;None;2ede2d5f45d1ec841b4a0931d8893481;True;0;False;gray;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;383;2683.435,858.5713;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.12;False;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;313;1927.275,984.7347;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0.1,0.2;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;305;2180.323,534.1116;Inherit;True;Property;_TextureSample1;Texture Sample 1;0;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Instance;76;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;317;2979.657,394.1439;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;387;2320.672,1453.138;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;345;3014.358,67.7265;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;388;2486.669,1458.674;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.3;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;318;3073.385,564.9489;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;314;2166.974,1030.757;Inherit;True;Property;_TextureSample2;Texture Sample 2;0;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Instance;76;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;325;2232.724,737.4359;Float;False;Constant;_GDividConst;GDividConst;8;0;Create;True;0;0;False;0;0.5;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;315;3018.719,675.8622;Inherit;False;Property;_BaseColor;BaseColor;6;0;Create;True;0;0;False;0;0,0,0,0;0.4823529,0.1372545,0.8705883,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;344;3170.852,259.3762;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;362;2890.093,891.4149;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;326;2536.724,721.4359;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;356;3359.743,762.0643;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;320;3475.827,427.7172;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;329;2235.904,1281.333;Float;False;Constant;_BDividConst;BDividConst;8;0;Create;True;0;0;False;0;0.1;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;319;3140.569,949.2686;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;389;2824.409,1730.613;Inherit;False;1552.225;492.5829;Comment;6;296;298;295;299;300;294;;1,1,1,1;0;0
Node;AmplifyShaderEditor.LerpOp;365;2652.488,1426.928;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;330;2609.545,1162.32;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;321;3672.616,730.0394;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;295;3440.952,1991.215;Inherit;False;Property;_LightColor;LightColor;2;0;Create;True;0;0;False;0;1,1,1,0;0.9915339,0.6273585,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;299;2907.665,1993.196;Inherit;True;Property;_TextureSample0;Texture Sample 0;4;0;Create;True;0;0;False;0;-1;None;1e72c2e016bfac74f80fbac5a691feb1;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FresnelNode;298;2874.409,1780.613;Inherit;True;Standard;WorldNormal;ViewDir;False;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;296;3865.981,2070.506;Float;False;Property;_LightPower;LightPower;3;0;Create;True;0;0;False;0;0.27;2.04;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;352;3219.636,1192.497;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;300;3367.811,1806.268;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;322;4001.608,854.5964;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;294;4214.634,1806.914;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;297;4338.89,998.2841;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;253;4675.289,1107.269;Float;False;BaseColor;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;391;4940.225,986.2697;Inherit;False;Property;_AddIntensity;AddIntensity;7;0;Create;True;0;0;False;0;1;2.491;1;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;252;4985.575,595.42;Inherit;True;253;BaseColor;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;392;5290.64,689.8727;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;5514.991,567.4377;Float;False;True;-1;6;ASEMaterialInspector;0;0;CustomLighting;Custom/QBHair_v2;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;ForwardOnly;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;0;4;10;25;True;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0.001;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;272;0;267;0
WireConnection;272;2;274;0
WireConnection;272;3;273;0
WireConnection;310;0;272;0
WireConnection;376;0;375;4
WireConnection;377;0;376;0
WireConnection;381;0;380;4
WireConnection;340;0;308;1
WireConnection;379;0;381;0
WireConnection;268;0;311;0
WireConnection;268;1;340;0
WireConnection;385;0;384;4
WireConnection;378;0;377;0
WireConnection;382;0;379;0
WireConnection;358;0;378;0
WireConnection;386;0;385;0
WireConnection;306;0;311;0
WireConnection;306;1;308;1
WireConnection;76;1;268;0
WireConnection;383;0;382;0
WireConnection;313;0;311;0
WireConnection;313;1;308;1
WireConnection;305;1;306;0
WireConnection;317;0;76;1
WireConnection;317;1;316;0
WireConnection;387;0;386;0
WireConnection;345;1;358;0
WireConnection;345;2;76;1
WireConnection;388;0;387;0
WireConnection;318;0;305;2
WireConnection;318;1;316;0
WireConnection;314;1;313;0
WireConnection;344;0;345;0
WireConnection;344;1;317;0
WireConnection;362;1;383;0
WireConnection;362;2;305;2
WireConnection;326;0;305;2
WireConnection;326;1;325;0
WireConnection;356;0;318;0
WireConnection;356;1;362;0
WireConnection;320;0;315;0
WireConnection;320;1;344;0
WireConnection;320;2;76;1
WireConnection;319;0;314;3
WireConnection;319;1;316;0
WireConnection;365;1;388;0
WireConnection;365;2;314;3
WireConnection;330;0;314;3
WireConnection;330;1;329;0
WireConnection;321;0;320;0
WireConnection;321;1;356;0
WireConnection;321;2;326;0
WireConnection;352;0;365;0
WireConnection;352;1;319;0
WireConnection;300;0;298;0
WireConnection;300;2;299;1
WireConnection;322;0;321;0
WireConnection;322;1;352;0
WireConnection;322;2;330;0
WireConnection;294;0;295;0
WireConnection;294;1;296;0
WireConnection;297;0;322;0
WireConnection;297;1;294;0
WireConnection;297;2;300;0
WireConnection;253;0;297;0
WireConnection;392;0;252;0
WireConnection;392;1;391;0
WireConnection;0;2;392;0
ASEEND*/
//CHKSM=F4D0ADCC2268020FF7B595D665AFA76838C65485